const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = environment => {
    return {
        mode: 'development',
        devtool: environment === 'production' ? 'none' : 'source-map',
        entry: "./src/index.js",
        output: {
            path: path.resolve(__dirname, "build/"),
            publicPath: "/",
            filename: "bundle.js"
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: "babel-loader",
                    options: { presets: ["@babel/env"] }
                },
                {
                    test: /\.(css|scss)$/,
                    use: ["style-loader", "css-loader", 'sass-loader']
                },
                {
                    test: /\.(png|svg|jpg|gif)$/,
                    use: [
                      'file-loader'
                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: [
                        'file-loader'
                    ]
                }
            ]
        },
        resolve: {
            modules: [
                path.resolve(__dirname, 'src'),
                'node_modules'
            ],
            extensions: ["*", ".js", ".jsx"]
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new CleanWebpackPlugin(['build']),
            new HtmlWebpackPlugin({
                title: 'Lally\’s Dog Walks',
                template: './src/index.html',
                filename: './index.html'
            })
        ]
    }
};