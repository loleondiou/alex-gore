import React from 'react';
import 'App.scss';
import Home from 'sections/home/Home.js';
import Services from 'sections/services/Services.js';
import WonderingWhere from 'sections/wonderingwhere/WonderingWhere.js';
import GetInTouch from "sections/getintouch/GetInTouch";
import ThanksForStoppingBy from 'sections/thanksforstoppingby/ThanksForStoppingBy';
import Header from 'sections/header/Header.js';

export default () => {
    return <div className='app'>
        <Header />
        <Home />
        <Services />
        <WonderingWhere />
        <GetInTouch />
        <ThanksForStoppingBy />

        <section className='oval' />
    </div>;
};