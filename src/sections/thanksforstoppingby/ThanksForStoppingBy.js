import React from 'react';
import './thanksForStoppingByStyles.scss'

const ThanksForStoppingBy = () => {
    return <div className='thanks-for-stopping-by'>
        <h3>Thanks for stopping by!</h3>
        <p>Copyright © 2019 Lally’s dog walks. All rights reserved.</p>
    </div>
};

export default ThanksForStoppingBy;