import React from 'react';
import './getInTouchStyles.scss'
import getInTouchImage from 'assets/getintouch/get_in_touch.jpg';

const GetInTouch = () => {
    return <div className='get-in-touch' id='get-in-touch'>
        <img alt='' src={getInTouchImage} />
        <div className='content'>
            <h2>Get in touch</h2>
            <p>
                Handing your dog over to a stranger is daunting, so let’s get acquainted and take it from there.
            </p>
            <email>lallys@someemail.com</email>
        </div>
    </div>
};

export default GetInTouch;