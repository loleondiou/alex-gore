import React from 'react';
import 'sections/wonderingwhere/wonderingWhere.scss'

const WonderingWhere = () => {
    return <div className='wondering-where' id='wondering-where'>
        <div className='title'>
            <h1>Wondering Where?</h1>
        </div>
        <img alt='Map and markers for areas covered.' src='https://maps.googleapis.com/maps/api/staticmap?center=51.429704,-0.302764&zoom=11&size=640x450&markers=51.414181,-0.301053&markers=51.462053,-0.303694&markers=51.401129,-0.259157&markers=51.407607,-0.230967&markers=51.427223,-0.227871&markers=51.337727,-0.267000&markers=51.394112,-0.302618&markers=51.413426,-0.366316&key=AIzaSyBVc_RMhK1RdGZKjQry1dbVKQBRwv8Efb0' />
    </div>
};

export default WonderingWhere;