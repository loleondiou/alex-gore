import React from 'react';
import imageAlexWithBailey from 'assets/services/alex-with-bailey.jpg';
import 'sections/services/servicesStyles.scss';

const Services = () => {
    return <div className='services' id='services'>
        <div className='services-content'>
            <img src={imageAlexWithBailey} alt='Alex with her dog Bailey.' />
            <div className='info'>
                <div className='info-content'>
                    <h2 className='title'>Services</h2>
                    <div className='intro'>
                        THINK OF A DECENT INTRO - Ask Alex
                    </div>
                    <div className='services-row'>
                        <div className='service left'>
                            <div className='service-content'>
                                <h5 className='title'>Dog walks</h5>
                                <div className='description'>
                                    Dog walks between 6am and 8pm on Fridays, Saturdays and Sundays.
                                </div>
                            </div>
                        </div>
                        <div className='service right'>
                            <div className='service-content'>
                                <h5 className='title'>Dog sitting</h5>
                                <div className='description'>
                                    Dog sitting upon request within the coverage area.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='services-row'>
                        <div className='service left'>
                            <div className='service-content'>
                                <h5 className='title'>Pick up and drop off</h5>
                                <div className='description'>
                                    Pick up and drop off of your dog to a location you decide within the coverage area.
                                </div>
                            </div>
                        </div>
                        <div className='service right'>
                            <div className='service-content'>
                                <h5 className='title'>Free trial walks</h5>
                                <div className='description'>
                                    Handing your dog over to a stranger is daunting! Have a free walk on me to get acquainted.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='services-row'>
                        <div className='service left'>
                            <div className='service-content'>
                                <h5 className='title'>Lenghty walks</h5>
                                <div className='description'>
                                    1-2 hour walks in Richmond Park or Wimbledon Common (maximum of 4 dogs).
                                </div>
                            </div>
                        </div>
                        <div className='service right'>
                            <div className='service-content'>
                                <h5 className='title'>One on one walks</h5>
                                <div className='description'>
                                    For that loan wolf who likes to have all the attention.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
};

export default Services;