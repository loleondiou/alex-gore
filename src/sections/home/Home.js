import React from 'react';
import 'sections/home/homeStyles.scss';
import heroImage from 'assets/home/home_hero_image.jpg';

const Home = () => {
   return (
       <section className='home' id='home'>
           <section className='content'>
               <h1 className='title'>Happy, fun walks for your dog!</h1>
               <img className='hero-image' src={heroImage} alt='Portrait of dogs Bailey and friend.' />
               <section className='introduction'>
                   My name is Alex (family nick name is Lally!). I am and have always been an animal lover, particularly dogs.
               </section>
               <section className='paragraph'>
                   My first dog at age 5 was a Potcake named Spot, even though he had no spots! Later in life came Charlie, a Collie/Lab cross rescued from Battersea. Now, my husband and I have been lucky enough to welcome Bailey into our lives. She is a mischievous but loving chocolate Labrador. Growing up with dogs means that I understand how they become part of the family.
               </section>
               <section className='quote'>
                   <section className='quote-content'>
                       I have always been passionate about dogs, even if they are not my own. I volunteered for the RSPCA as it is an important charity to me. I walked several rescue dogs to ensure that they were happy and exercised.
                   </section>
               </section>
               <section className='paragraph last'>
                   Over the years, I have also walked various breeds of dog including an Australian Cattle dog, Wink, a Border Terrier named Indie and Dustin, a French Bulldog. For me, walking with dogs is a wonderful and happy experience that I hope to be able to share with your dog.
               </section>
           </section>
       </section>
   );
};

export default Home;