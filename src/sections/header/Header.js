import React from "react";
import 'sections/header/headerStyles.scss'

const Header = () => {
    return <header className='header' id='header'>
        <div className='logo'>
            <div className='logo-content'> Lally’s dog walks</div>
        </div>
        <div className='nav'>
            <div className='nav-content'>
                <a href='#home'>Home</a>
                <a href='#services'>Services</a>
                <a href='#wondering-where'>Where</a>
                <a href='#get-in-touch'>Contact</a>
            </div>
        </div>
    </header>
};

export default Header;